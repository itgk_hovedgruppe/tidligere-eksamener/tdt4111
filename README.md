# Gamle eksamener i TDT4111

| Mappe     | Eksamen                |          Dato |
| :-------- | ---------------------- | ------------: |
| `2022-1a` | Ordinære eksamen       | desember 2022 |
| `2022-1b` | Ekstra ordinær eksamen |   januar 2023 |
| `2022-2`  | Konteeksamen           |   august 2023 |
| `2023-1`  | Ordinær eksamen        | desember 2023 |
| `2023-2`  | Konteeksamen           |   august 2023 |
| `2024-1`  | Ordinær eksamen        | november 2024 |

Ergo er alle som starter med `2022` fra det kullet, men ikke nødvendigvis holdt det kalenderåret.
