'''
Denne funksjonen skal gjøre det samme som max-funksjonen for lister:
Den mottar en liste og returnerer den største verdien i denne.
Eksempel på kjøring:

>>> maks([1, 2, 3, 6, 5, 4])
6
'''

def maks(liste):
    største = liste[-1]  # [-1]
    for item in liste[:-1]:  # liste[:-1]
        if item < største:  # <
            pass
        else:
            største = item  # item
    return største

print(maks([1, 2, 3, 6, 5, 4]))
