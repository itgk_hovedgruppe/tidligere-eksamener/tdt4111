'''
Organisasjonen din vil vise hvordan de har medlemmer fra hele verden. 
De har en database som kan gi oss en dict med lister, hvor hvert felt er 
navnet til personen, og listen inneholder alltid, bare elementene 
[alder, land, stilling] i akkurat denne rekkefølgen. Dicten kan se ut som følger:

members = {
    'Anne': [35, 'Norge', 'CEO'],
    'Bjarne': [55, 'Norge', 'sekretær'],
    'Carl': [45, 'USA', 'CEO'],
    'Diana': [28, 'UK', 'vaktmester']
... <flere medlemmer> ...
}


For å få oversikt over medlemsmassens struktur når det gjelder alder ønsker en 
å få laget en funksjon snitt_alder(members, member) som gjør følgende:

- *members* er dicten slik den er spesifisert over
- *member* er navnet på et medlem
- Funksjonen skal returnere gjennomsnittsalderen for alle medlemmer som har samme
  rolle i organisasjonen som *member* har.
- Det er ikke så viktig å være fullstendig nøyaktig her. Det er derfor nok å returnere
  gjennomsnittet som nærmeste *heltall*.
- Du kan forutsette at navnet til member finnes i databasen, og at alle medlemmer har 
  korrekt utfylte data. 

Eksempel på kjøring:

>>> snitt_alder(members, "Anne")
40

>>> # Siden Anne er CEO, det er totalt 2 CEO i medlemsmassen, og snittet på dem er 40.

Kommenter eventuelle antagelser du tar. 

Skriv ditt svar her


'''

members = {
    'Anne': [35, 'Norge', 'CEO'],
    'Bjarne': [55, 'Norge', 'sekretær'],
    'Carl': [45, 'USA', 'CEO'],
    'Diana': [28, 'UK', 'vaktmester']
}

def snitt_alder(members, member):
    total_alder = 0
    antall_stilling = 0

    # Først, hvilken stillingsbetegnelse er det vi leter etter?
    stilling = members[member][2]

    # Så må vi lete igjennom alle verdier i medlemsdatabasen for å se etter medlemmer
    # som har samme stillingsbetegnelse som den vi nå fant. Hvis de har, legg til alder
    # og øk antall med én.

    for alder, land, tittel in members.values():
        if tittel == stilling:
            total_alder += alder
            antall_stilling += 1

    return int(total_alder/antall_stilling)

print(snitt_alder(members, 'Anne'))
