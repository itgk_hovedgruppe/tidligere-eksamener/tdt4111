def antall_vokal_konsonant(filnavn):
    # Les fil
    with open(filnavn) as f:
        tekst = f.read()

    # Alternativ fillesing
    # f = open(filnavn)
    # tekst = f.read()
    # f.close()

    # Split til ord og ignorer irrelevante linjer
    linjer = tekst.split('\n')
    relevante_ord = []
    for linje in linjer:
        if not linje.startswith('T: ') and not linje.startswith('F: '):
            relevante_ord += linje.split()

    # Fiks ord
    fiksede_ord = []
    for ord in relevante_ord:
        ord = ord.replace('aa', 'å')
        ord = ord.lower()
        fiksede_ord.append(ord)

    # Tell antall linjer
    vokaler = 'aeiouyæøå'
    antall_riktige_ord = 0
    for ord in fiksede_ord:
        if (len(ord) > 1) and (ord[0] in vokaler) and (ord[1] not in vokaler):
            antall_riktige_ord += 1
    return antall_riktige_ord


print(antall_vokal_konsonant('dikt.txt'))
