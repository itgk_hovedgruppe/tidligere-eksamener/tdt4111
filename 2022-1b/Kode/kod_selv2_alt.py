vokaler = 'aeiouyæøå'

def antall_vokal_konsonant(filnavn):
    with open(filnavn, encoding = "utf-8") as f:
        tekst = f.read()
    linjer = tekst.split('\n')

    antall_vokal_konsonant_ord = 0

    for linje in linjer:
        if linje[:2] in ['T:', 'F:']: 
            continue

        linje = linje.replace('aa', 'a')
        linje = linje.replace('Aa','å')

        for ord in linje.split():
            if ord[0].lower() in vokaler and len(ord) > 1 and ord[1] not in vokaler:
                antall_vokal_konsonant_ord += 1
    return antall_vokal_konsonant_ord

print(antall_vokal_konsonant('dikt.txt'))
