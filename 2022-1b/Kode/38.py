'''
Bedriften din vil gi fordeler til langtidskunder, og du vil derfor sende dem et gavekort.
Følgende regler gjelder:
- Hvis de har vært kunder i 3 eller færre år får de ikke noe gavekort.
- Hvis de har vært kunder lenger enn 3 år, får et gavekort på 100 kroner.
- Hvis de har vært kunder i over 10 år, får de et gavekort på ti ganger antall år de har vært kunde. Vi regner bare i hele år.
'''

def rabatt(medlemskap_i_år):
    if medlemskap_i_år > 10: 
        verdi = 10 * medlemskap_i_år 
    elif medlemskap_i_år < 3:
        return 0  
    else:
        verdi = 100  
    return verdi

print(rabatt(0), 0)
print(rabatt(3), 0)
print(rabatt(5), 100)
print(rabatt(10), 100)
print(rabatt(11), 110)

# decoys:
# - medlemskap_i_år <= 10


# Spørsmål: Kan det misforstås til at 3 år og 6 måneder er mer enn 3 år?
