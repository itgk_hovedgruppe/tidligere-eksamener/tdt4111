'''
Denne koden estimerer hvor mange ganger vi kaste en 6-sidet terning for å komme over
grenseverdien (grense = 20). Et eksempel på en serie terningkast kan være:
Kast-nr.        1   2   3   4   5   6   7   8   ..
Kast            4   3   5   5   1   4   6   2   ..
Kumulativ sum   4   7   12  17  18  22  28  30  ..
Det tok altså 6 kast før vi fikk en kumulativ sum på over grensen på 20.
I dette tilfellet skal funksjonen i del 2 returnere 6. Siden det er
tilfeldige terningkast vil antall terningkast som er nødvendig variere.

Koden består av 3 deler (1, 2 og 3).
- Del 1 er import av bruk av random, til å lage en terningkastfunksjon.
- Del 2 gjentar terningkast til summen av kast er over grenseverdien.
- Del 3 kjører funksjonen i del 2 gjentatte ganger og finner gjennomsnittet
    av resultatene den returnerer.

Koden fungerer som den skal, men deler er skrevet på ulogiske måter!

Plasser kodeblokkene slik at programmet funker riktig.
'''


# Del 1
import random

def kast_terning():
    return random.randint(1, 6) # randint, 1


# Del 2
def finn_antall_kast(grense):
    totalsum = 0  # 0
    antall_kast = 0
    while True:  # True
        if totalsum > grense:  # totalsum > grense
            return antall_kast
        totalsum += kast_terning()  # kast_terning()
        antall_kast += 1  # 1


# Del 3
grense = 20
antall_kjøringer = 1_000

simuleringer = []
for i in range(antall_kjøringer):
    simuleringer.append(finn_antall_kast(grense))
forventet = sum(simuleringer) / len(simuleringer)
print(forventet)
