'''
Denne koden skal spørre brukeren om hvor kaldt det er (i celsius), og så
konvertere tempreraturen til farenheit og skrive ut en forklarende setning.

Eksempel på kjøring:

>>> how_cold_is_it()
Temp outside (C)? 13
It's 13.0C outside , which is 55.4f in freedom units.

'''

def c2f(c):
    return c * 9/5 + 32

def f2c(f):
    return (f - 32) * 5/9

def how_cold_is_it():
    celsius = float(input('Temp outside (C)? '))  # float, input
    farenheit = c2f(celsius)  # farenheit, c2f, celsius
    text = f'It\'s {celsius}C outside , which'  # It\'s
    return text + f' is {farenheit}f in freedom units.'  # text + f

something = how_cold_is_it()  # _
print(something)  # something
