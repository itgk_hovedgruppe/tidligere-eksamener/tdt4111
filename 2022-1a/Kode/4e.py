def finn_antall_år(startsum, sluttsum, rente):
    kroner_i_banken = startsum
    antall_år = 0
    while kroner_i_banken < sluttsum:
        kroner_i_banken = kroner_i_banken * rente
        antall_år += 1
    return antall_år


print(finn_antall_år(10_000, 20_000, 1.05))
