def les_fra_fil(filnavn):
    with open(filnavn, 'r') as f:
        tekst = f.read()
    return tekst.split('\n')


def tell_prosent_00(linjer):
    antall = 0
    for linje in linjer:
        if linje.endswith('.00'):
            antall += 1
    return antall / len(linjer) * 100


linjer = les_fra_fil('data.txt')
print(linjer)
assert linjer == ['100.27', '2300.00', '123.45', '420.69'], f'{linjer}'

prosent = tell_prosent_00(linjer)
print(prosent)
assert prosent == 25.0
