def gjør_ting(kake):

    y = 0

    for nam in kake.values():

        y += nam

    return y


# 1
print(gjør_ting({}))

# 2
print(gjør_ting({'hei': 100, 'hallo': 120}))

# 3
print(gjør_ting({'hei': 100, 'sann': 200, 'hopp': 50, 'sann': 200}))
