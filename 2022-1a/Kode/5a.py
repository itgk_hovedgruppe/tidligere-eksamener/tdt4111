import numpy as np
import copy


# Alterative 1 - NumPy
def til_kroner_v1(utgifter):
    return np.array(utgifter) * 1_000


# Alternative 2 - Indexes
def til_kroner_v2(utgifter):
    for i in range(len(utgifter)):
        for j in range(len(utgifter[i])):
            utgifter[i][j] *= 1_000
    return utgifter


# Alternative 3 - Row append
def til_kroner_v3(utgifter):
    ny_tabell = []
    for rad in utgifter:
        ny_rad = []
        for tall in rad:
            ny_rad.append(tall * 1_000)
        ny_tabell.append(ny_rad)
    return ny_tabell


# Data and expected results
utgifter = [
    # lønn    utstyr    annet
    [120,     1007,     320],   # selskap 1
    [290,      500,     300],   # selskap 2
    [ 50,      500,      70]    # selskap 3
]
print(f'Type: {type(utgifter)}, Values: {utgifter}')

utgifter_i_kroner = [
    #  lønn       utstyr       annet
    [120000,     1007000,     320000],   # selskap 1
    [290000,      500000,     300000],   # selskap 2
    [ 50000,      500000,      70000]    # selskap 3
]
print(f'Type: {type(utgifter_i_kroner)}, Values: {utgifter_i_kroner}')

utgift_array = np.array(utgifter_i_kroner)
print(f'Type: {type(utgift_array)}, Values: {utgift_array}')

# Gather functions and run tests
functions = [til_kroner_v1, til_kroner_v2, til_kroner_v3]
for func in functions:
    result = func(copy.deepcopy(utgifter))
    print(f'Func: {func.__name__}, Type: {type(result)}, Values: {result}')
    if type(result) is type(utgifter_i_kroner):
        assert result == utgifter_i_kroner
    elif type(result) is type(utgift_array):
        assert result.shape == utgift_array.shape
        np.array_equal(result, utgift_array)
    else:
        assert False
